
ALTER TABLE MATRICE DROP CONSTRAINT fk_matrice_id_mot_1 ;
ALTER TABLE MATRICE DROP CONSTRAINT fk_matrice_id_mot_2 ;

DROP TABLE dictionnaire;
DROP TABLE matrice;



CREATE TABLE dictionnaire (
    id_mot  NUMBER(6,0) CONSTRAINT nn_dictionnaire_id_mot NOT NULL,
    mot     VARCHAR2(20)  CONSTRAINT nn_dictionnaire_mot NOT NULL,
    
    CONSTRAINT us_dictionnaire_id_mot UNIQUE(id_mot),
    CONSTRAINT us_dictionnaire_mot UNIQUE(mot)
);

CREATE TABLE matrice(
    id_mot_1    NUMBER(6,0) CONSTRAINT  nn_matrice_id_mot_1 NOT NULL,
    id_mot_2    NUMBER(6,0) CONSTRAINT  nn_matrice_id_mot_2 NOT NULL,
    fenetre     NUMBER(6,0) CONSTRAINT nn_matrice_fenetre NOT NULL,
    coocs       NUMBER(6,0) CONSTRAINT nn_matrice_coocs NOT NULL

);



ALTER TABLE matrice 
ADD ( CONSTRAINT    uc_matrice  UNIQUE (id_mot_1, id_mot_2, fenetre),
CONSTRAINT fk_matrice_id_mot_1 FOREIGN KEY ( id_mot_1) REFERENCES dictionnaire(id_mot),
CONSTRAINT fk_matrice_id_mot_2 FOREIGN KEY ( id_mot_2) REFERENCES dictionnaire(id_mot));