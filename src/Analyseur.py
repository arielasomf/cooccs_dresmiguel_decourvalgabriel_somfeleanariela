import sys
import re
import numpy as np


class Analyseur:
    def __init__(self, encodage,fenetre,connexion):
        self.chemin = None
        self.encodage = encodage
        self.texte = []
        self.dict={}
        self.matrice=[]
        self.fenetre=fenetre
        self.listeNouvMots=[]
        self.dCoocs={}
        self.connexion=connexion
        
        
        
    def lire(self,ch,enc):    
        try:
            f=open(ch,'r',encoding=enc)          
            s=f.read()
            return s.lower()
        except:
            print("Erreur lors de la lecture du fichier texte, veuillez vérifier qu'il s'agit d'un fichier existant avec le bon encodage.")
            sys.exit()
        else:
            f.close()
    
    def trouverMots(self,s):
        return re.findall('\w+', s)
    

    def ajouterMotsDict(self):
        nb=len(self.dict)
        for mot in self.texte:
            if mot not in self.dict:
                self.listeNouvMots.append(tuple((nb,mot)))
                self.dict[mot] = nb
                nb+=1   
                
    def matrixToTuple(self):

        length=range(len(self.matrice))
        self.listeMatrixtoTuple=np.zeros(length)
        for row in length:
            for x in range(row):
                if(self.matrice[row][x]!=0):
                    self.listeMatrixtoTuple[row+x]=(tuple((
                        row,
                        x,
                        self.fenetre,
                        self.matrice[row][x])))
                    

        for row in range(len(self.matrice)):
            for column in range(len(self.matrice)):
                if(self.matrice[row][column]!=0):
                    self.listeMatrixtoTuple.append(tuple((row**2,column**2,self.fenetre**2,self.matrice[row][column]**2)))

 
        
    def setup(self,mode,dBD,chemin,dict):
        self.dict = dict
        if(mode=="entrainement"):
            self.chemin=chemin
            self.texte= self.trouverMots( self.lire(self.chemin, self.encodage))
            
        else:
            self.dCoocs=dBD
            self.dictCoocsToMatrice()
            
            
        self.ajouterMotsDict()
        self.constructCoocDic()
        print("done")
        
    
        
    def constructCoocDic1(self):
        T2=self.fenetre//2
        for i in range(T2,len(self.texte)-T2):
            imot=self.dict[self.texte[i]]
            for j in range(1,T2+1):
                if j<len(self.texte):
                    icoocs=self.dict[self.texte[i+j]]
                    if imot<=icoocs:
                        if(imot,icoocs,self.fenetre) not in self.dCoocs:
                            self.dCoocs[(imot,icoocs,self.fenetre)]=1
                        else:
                            self.dCoocs[(imot,icoocs,self.fenetre)]+=1
                else:
                    icoocs=self.dict[self.texte[i-j]]
                    if(imot<=icoocs):
                        if(imot,icoocs,self.fenetre) not in self.dCoocs:
                            self.dCoocs[(imot,icoocs,self.fenetre)]=1
                        else:
                            self.dCoocs[(imot,icoocs,self.fenetre)]+=1
                            
    
    def constructCoocDic(self):
        T2=self.fenetre//2
        for i in range(len(self.texte)):
            imot=self.dict[self.texte[i]]
            for j in range(1,T2+1):
                if i+j < len(self.texte):
                    icoocs=self.dict[self.texte[i+j]]
                    if imot<=icoocs:
                        if(imot,icoocs,self.fenetre) not in self.dCoocs:
                            self.dCoocs[(imot,icoocs,self.fenetre)]=1
                        else:
                            self.dCoocs[(imot,icoocs,self.fenetre)]+=1
                if i-j >= 0:
                    icoocs=self.dict[self.texte[i-j]]
                    if(imot<=icoocs):
                        if(imot,icoocs,self.fenetre) not in self.dCoocs:
                            self.dCoocs[(imot,icoocs,self.fenetre)]=1
                        else:
                            self.dCoocs[(imot,icoocs,self.fenetre)]+=1
                      

    
    def dictCoocsToMatrice(self):
        m=np.zeros((len(self.dict),len(self.dict)))
        for (idword,idword2,tfen),coocs in self.dCoocs.items():
            m[idword][idword2]=coocs
            m[idword2][idword]=coocs
        self.matrice = m

   

