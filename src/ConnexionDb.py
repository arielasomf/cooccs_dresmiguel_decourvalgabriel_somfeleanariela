from mdp import mdp
import cx_Oracle


class ConnexionDb():
    instance=None
    cur=None
    
    def __init__(self):
        dsn=cx_Oracle.makedsn('delta', 1521, 'decinfo')
        chaineConnexion = 'e1045641' + '/' + mdp + '@' + dsn
        ConnexionDb.instance=cx_Oracle.connect(chaineConnexion)
        ConnexionDb.cur=ConnexionDb.instance.cursor()
            
    def getInstance(self):
        if ConnexionDb.instance==None:
            ConnexionDb()
            
        return ConnexionDb.instance
    

    def getDict(self):
        ConnexionDb.cur.execute('SELECT mot, id_mot FROM dictionnaire')
        rangees = ConnexionDb.cur.fetchall()
        
        return dict(rangees)
        

    def updateDict(self,liste):
        enonce = 'INSERT INTO dictionnaire(id_mot,mot) VALUES (:1,:2)'
        ConnexionDb.cur.executemany(enonce,liste)
        ConnexionDb.instance.commit()
    
    def getMatrice(self,window):
        dBD={}
        ConnexionDb.cur.execute('SELECT * FROM MATRICE WHERE FENETRE = '+ str(window)+'')
        for idword,idword2,window,coocs  in ConnexionDb.cur.fetchall():
            dBD[(idword,idword2,window)]=coocs;   
        return dBD
        
    def updateMatrice(self, dCoocs, fenetre):
        insert = []
        update = []
        BDMatrice = ConnexionDb().getMatrice(fenetre)
        
        enonceUpdate = 'UPDATE matrice SET coocs = :1 WHERE id_mot_1 = :2 AND id_mot_2 = :3 AND fenetre = :4'
        enonceInsert = 'INSERT INTO matrice(id_mot_1, id_mot_2, fenetre, coocs) VALUES (:1, :2, :3, :4)'
        
        for(mot1, mot2, tfen), frequence in dCoocs.items():
            if(mot1, mot2, tfen) in BDMatrice :
                update.append(((frequence + BDMatrice[mot1,mot2,tfen]), mot1, mot2, tfen))
            else:
                insert.append((mot1, mot2, tfen, frequence))
                
        ConnexionDb.cur.executemany(enonceUpdate, update)
        ConnexionDb.cur.executemany(enonceInsert, insert)
        ConnexionDb.instance.commit()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    

    

        