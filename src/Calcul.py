import numpy as np
import Util
import re

class Calcul:
    def __init__(self):
        self.stopList={  "le":0,"la":1,"de":2,"à":3,"qu":4,"et":5,"un":6,"il":7,"elle":8,"d":9,"que":10,
                         "l":11,"vous":12,"je":13,"tu":14,"nous":15,"ils":16,"elles":17,"mon":18,"ma":19,
                         "notre":20,"votre":21,"a":22,"par":23,"pour":24,"sans":25,"mais":26,"ou":27,"où":28,
                         "donc":29,"or":30,"ni":31,"car":32,"en":33,"s":34,"les":35,"qui":36,"une":37,"se":38,
                         "lui":39,"son":40,"ne":41,"du":42,"comme":43,"n":44,"se":45,"ce":46,"pas":47,"dans":48,
                         "sur":49,"m":50,"c":51,"au":52,"des":53,"mes":54,"avec":55,"ai":56,"on":57,"mien":58,
                         "mienne":59,"aux":60
                         }
        self.ps=0
        self.cb=2
        self.ls=1
        self.scores=[]
        
   
        
         
    def calcule(self, motR,fonc,dicti,mat):
        scores=[]
        if motR not in dicti:
            scores.append((0,"le mot n'existe pas"))
        else:
            for mot, index in dicti.items():
                if mot != motR and mot not in self.stopList:
                    score=fonc(mat[dicti[motR]],mat[index])
                    scores.append((score,mot))
        return scores
    
    def trouverRes(self,mot,meth,dicti,mat):
        if meth==self.ps:
            self.scores=sorted(self.calcule(mot,self.produitScalaire,dicti,mat),key=self.getKey,reverse=True)
        elif meth == self.ls:
            self.scores=sorted(self.calcule(mot,self.leastSquare,dicti,mat),key=self.getKey)
        elif meth == self.cb:
            self.scores=sorted(self.calcule(mot,self.cityBlock,dicti,mat),key=self.getKey)
     
            
    def leastSquare(self, u,v):
        return np.sum((u-v)**2)
    def cityBlock(self,u,v):
        return np.sum((abs(u-v)))
    def produitScalaire(self, u,v):
        return np.dot(u,v)
    def getKey(self,item):
        return item[0]
    
    
       