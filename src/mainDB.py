from ConnexionDb import *
from Analyseur import *
from Calcul import *


def main():

    e,r,fenetre,encodage,chemin=Util.loadArgs().values() 
    connexion = ConnexionDb()
    
    if(e):
        a=Analyseur(encodage,fenetre,connexion) 
        a.setup(Util.mode["e"],connexion.getMatrice(fenetre),chemin,connexion.getDict())
        connexion.updateDict(a.listeNouvMots)
        connexion.updateMatrice(a.dCoocs,fenetre)
        
    else:

        a=Analyseur(encodage,fenetre,connexion) 
        a.setup(Util.mode["r"],connexion.getMatrice(fenetre),None,connexion.getDict())
        c=Calcul()
        bonnerep=False
        while not bonnerep:
            bonnerep=Util.displaySynonyms(c,a)
    
    return 0

if __name__ == '__main__':
    sys.exit(main())