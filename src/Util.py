import argparse
import re

mode={"e":"entrainement","r":"recherche"}

def loadArgs():
    fenetre = None;
    encodage = None;
    chemin = None;
    
    parser = argparse.ArgumentParser(description='Calculer des synonymes basés sur des co-occurences qui sont stockées dans une BD oracle.')
    group = parser.add_mutually_exclusive_group(required=True)
    
    group.add_argument('-e', action='store_true',
                        help='Spécifie si on veut utiliser ou non des textes d\'entrainement.')
    
    group.add_argument('-r', action='store_false',
                       help='Spécifie si on veut faire une recherche ou non.')
    
    parser.add_argument('-t', action='store', dest='fenetre', type=int,
                            help='La taille de la fenêtre.')
    
    parser.add_argument('--enc', action='store', dest='encodage',
                         help='L\'encodage à utiliser.')

    parser.add_argument('--chemin', action='store', dest='chemin',
                        help='Emmagasiner le chemin du texte d\'entrainement.')
    
    if parser.parse_args().e == True and (parser.parse_args().encodage == None or 
                                          parser.parse_args().chemin == None):
        parser.error('Erreur, les arguments --enc et --chemin doivent êtres spécifiés avec l\'option -e.')

    if parser.parse_args().fenetre == None:
        parser.error('Erreur, vous devez spécifier une taille de fenêtre.')

    
    return vars(parser.parse_args())



def afficherMenu():
    print("Entrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul,"
               "i.e. produit scalaire: 0, least-square: 1, city-block:2 "
               "\n Tapez q pour quitter.")
    reponse=None
    reponse=input()
    return reponse
    
def displaySynonyms(c,a):
        quit=False    
        reponse=afficherMenu()
        reponse= re.split('\s+', reponse)
        if(reponse[0] == 'q'):
            quit=True#used to split answer into the three elements required
        else:
            mot=reponse[0]
            synonymes=int( reponse[1])
            methode=int(reponse[2])
            if(methode<3 and methode >=0):
                c.trouverRes(mot,methode,a.dict,a.matrice)
                if(len(range(synonymes))<len(c.scores)):
                    for x in range(synonymes):
                        print('{} --> {}\n'.format(c.scores[x][1],c.scores[x][0]))
                else:
                    for x in range(len(c.scores)):
                        print('{} --> {}\n'.format(c.scores[x][1],c.scores[x][0]))
        return quit
