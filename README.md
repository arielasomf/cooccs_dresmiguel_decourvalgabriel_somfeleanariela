# Cooccs_DresMiguel_DeCourvalGabriel_SomfeleanAriela

____   ____             __                                   .___       _________                                                                             
\   \ /   /____   _____/  |_  ____  __ _________  ______   __| _/____   \_   ___ \  ____   ____   ____  ____  __ _________   ____   ____   ____  ____   ______
 \   Y   // __ \_/ ___\   __\/ __ \|  |  \_  __ \/  ___/  / __ |/ __ \  /    \  \/ /  _ \ /  _ \_/ ___\/ ___\|  |  \_  __ \_/ __ \ /    \_/ ___\/ __ \ /  ___/
  \     /\  ___/\  \___|  | \  ___/|  |  /|  | \/\___ \  / /_/ \  ___/  \     \___(  <_> |  <_> )  \__\  \___|  |  /|  | \/\  ___/|   |  \  \__\  ___/ \___ \ 
   \___/  \___  >\___  >__|  \___  >____/ |__|  /____  > \____ |\___  >  \______  /\____/ \____/ \___  >___  >____/ |__|    \___  >___|  /\___  >___  >____  >
              \/     \/          \/                  \/       \/    \/          \/                   \/    \/                   \/     \/     \/    \/     \/ 

Fait par: Miguel Dres-Barrientos, Ariela Teodora Somfelean, Gabriel de Courval-Paré
Remis à: Pierre-Paul Monty
Dans le cadre du cours B62

===Utilisation===

Voici comment utiliser ce programme:

1.	Exécuter le script tablesSQL.sql localisé dans le dossier script. Celui-ci préparera une base de données oracle
	afin d'accueillir les données requises au bon fonctionnement du programme.

2.	Se positionner dans le répertoire source à l'aide de l'invite de commandes windows et
	ensuite taper mainDB.py suivi des arguments suivants:
															-e pour le mode entraînement
																-t pour la taille de la fenêtre
																--enc pour le type d'encodage
																--chemin pour le chemin vers le texte d'entraînement
																
															-r pour le mode recherche de synonymes
																-t pour la taille de la fenêtre
																
3.	Dans le cas du mode d'entraînement, il y aura un délai avant que le texte soit analysé et envoyé vers la base
	de données. Le message "done" apparaîtra lorsque ce processus sera terminé. Attendez en moyenne entre 2 et 12 secondes.
	
	Dans le cas du mode de recherche, vous serez interpellé à spéficier un mot suivi du nombre de synonymes que vous désirez
	ainsi que d'une méthode de score parmis les suivantes:
															-0 ---> Produit scalaire
															-1 ---> Carré moindre
															-2 ---> City block
															
	Le tout doit être séparé par des espaces. Ex: "veuve 7 2" (ignorer les guillements double).
	Une fois que vous aurez terminé, vous pouvez quitter en entrant la lettre 'q'.
	
4. Bonne recherche de synonymes!
	